#!/usr/bin/env python3
import board
import flask
import neopixel
import os
import threading
import time
import json

from flask import render_template, request
 
pixel_pin = board.D18 # Must be one of D10, D12, D18 or D21
num_pixels = 150
ORDER = neopixel.GRBW
SWAPGB = True
pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness=0.2, auto_write=False,
                           pixel_order=ORDER)

app = flask.Flask(__name__)
app.config["DEBUG"] = True

ACTION="off"
STATE=""
thread=None

def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos*3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos*3)
        g = 0
        b = int(pos*3)
    else:
        pos -= 170
        r = 0
        g = int(pos*3)
        b = int(255 - pos*3)
    if ORDER == neopixel.RGB or ORDER == neopixel.GRB:
        if SWAPGB:
            return (r, b, g)
        return (r, g, b)
    else:
        if SWAPGB:
            return (r, b, g, 0)
        return (r, g, b, 0)

def pixels_rainbow(wait):
    for j in range(255):
        for i in range(num_pixels):
            pixel_index = (i * 256 // num_pixels) + j
            pixels[i] = wheel(pixel_index & 255)
        pixels.show()
        time.sleep(float(wait))

def pixels_off():
    pixels.fill((0, 0, 0, 0))
    pixels.show()
    time.sleep(0.5)

def pixels_on():
    pixels.fill((255, 255, 255, 255))
    pixels.show()
    time.sleep(0.5)

def pixels_set_one(pix, red, green, blue, white):
    if SWAPGB:
        (green, blue) = (blue, green)
    pixels[int(pix)] = (int(red), int(green), int(blue), int(white))
    pixels.show()
    time.sleep(0.5)

def pixels_set_all(red, green, blue, white):
    if SWAPGB:
        (green, blue) = (blue, green)
    pixels.fill((int(red), int(green), int(blue), int(white)))
    pixels.show()
    time.sleep(0.5)

def pixels_strobe(red, green, blue, white, delay):
    if SWAPGB:
        (green, blue) = (blue, green)
    pixels.fill((int(red), int(green), int(blue), int(white)))
    pixels.show()
    time.sleep(float(delay))
    pixels.fill((0, 0, 0, 0))
    pixels.show()
    time.sleep(float(delay))

def pixels_setmany(desired_state):
    global ACTION
    try:
        dstate = json.loads(desired_state)
    except:
        ACTION="off"
        return
    if type(dstate) == list:
        j = 0
        for i in dstate:
            pixels[j] = i
            j = j + 1
    elif type(dstate) == dict:
        for i in dstate:
            led_spec = i
            colour = dstate[i]
            led_list = []
            if ":" in led_spec:
                ledfrom, ledto = led_spec.split(":")
                led_list = range(int(ledfrom), int(ledto) + 1)
            else:
                led_list = [int(led_spec)]

            for led in led_list:
                if SWAPGB:
                    (colour[1], colour[2]) = (colour[2], colour[1])
                pixels[led] = colour
    else:
        ACTION="off"
        
    pixels.show()
    time.sleep(0.5)

def led_loop():
    while True:
        stateargs = STATE.split("/")
        if ACTION=="off":
            pixels_off()
            print("off")
        elif ACTION=="on":
            pixels_on()
            print("on")
        elif ACTION=="rainbow":
            print("rainbow" + stateargs[0])
            pixels_rainbow(stateargs[0])
        elif ACTION=="set_one":
            print("set_one"+stateargs[0])
            pixels_set_one(stateargs[0], stateargs[1], stateargs[2], stateargs[3], stateargs[4])
        elif ACTION=="set_all":
            print("set_all"+stateargs[0])
            pixels_set_all(stateargs[0], stateargs[1], stateargs[2], stateargs[3])
        elif ACTION=="strobe":
            print("strobe"+stateargs[0])
            pixels_strobe(stateargs[0], stateargs[1], stateargs[2], stateargs[3], stateargs[4])
        elif ACTION=="set_many":
            print("set_many"+stateargs[0])
            pixels_setmany(stateargs[0])
        else:
            print("NO")


@app.route('/', methods=['GET'])
def home():
    return render_template('home.html', what=STATE, pixels=pixels)

@app.route('/api/v1/status', methods=['GET'])
def status():
    return ACTION+" "+STATE

@app.route('/api/v1/off', methods=['GET'])
def off():
    global ACTION
    ACTION="off"

    return "OFF\n"

@app.route('/api/v1/on', methods=['GET'])
def on():
    global ACTION
    ACTION="on"

    return "ON\n"

@app.route('/api/v1/rainbow/<speed>', methods=['GET'])
def rainbow(speed):
    global STATE
    global ACTION
    STATE=speed
    ACTION="rainbow"

    return "Rainbow\n"

@app.route('/api/v1/strobe/<red>/<blue>/<green>/<white>/<frequency>', methods=['GET'])
def strobe(red, blue, green, white, frequency):
    global STATE
    global ACTION
    STATE=red+"/"+green+"/"+blue+"/"+white+"/"+frequency
    ACTION="strobe"

    return "Strobe\n"

@app.route('/api/v1/set_all/<red>/<blue>/<green>/<white>', methods=['GET'])
def set_all(red, blue, green, white):
    global STATE
    global ACTION
    STATE=red+"/"+green+"/"+blue+"/"+white
    ACTION="set_all"

    return "Set All\n"

@app.route('/api/v1/set_one/<led_id>/<red>/<blue>/<green>/<white>', methods=['GET'])
def set_one(led_id, red, blue, green, white):
    global STATE
    global ACTION
    STATE=led_id+"/"+red+"/"+green+"/"+blue+"/"+white
    ACTION="set_one"

    return "Set One\n"

@app.route('/api/v1/set_many', methods=['POST'])
def set_many():
    global STATE
    global ACTION
    STATE = str(request.data.decode('UTF-8') or list(request.form.keys())[0])
    ACTION="set_many"

    return "Set Many\n"


if __name__ == "__main__":
    if not thread and os.environ.get("WERKZEUG_RUN_MAIN") == "true":
        print("Creating Thread")
        thread = threading.Thread(target=led_loop, daemon=True)
        thread.start()
    app.run(host="0.0.0.0")
